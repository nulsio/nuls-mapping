package io.nuls.mapping.business;

import io.nuls.mapping.dao.mapper.AddressMapper;
import io.nuls.mapping.dao.util.SearchOperator;
import io.nuls.mapping.dao.util.Searchable;
import io.nuls.mapping.entity.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AddressBusiness {

    @Autowired
    private AddressMapper addressMapper;


    /**
     * 用户绑定nuls地址，需要给用户生成新的ETH地址
     * @param address
     */
    @Transactional
    public Address save(Address address) {
        //TODO 获取以太坊地址
//        String ethAddress = ETHUtil.getEthAddress();
//        address.setEthAddress(ethAddress);
        addressMapper.insert(address);
        return address;
    }

    /**
     * 根据nuls地址，获取账户
     * @param nulsAddress
     * @return
     */
    public Address getByNulsAddress(String nulsAddress) {
        Searchable searchable = new Searchable();
        searchable.addCondition("nuls_address", SearchOperator.eq, nulsAddress);
        return addressMapper.selectBySearchable(searchable);
    }
}
