package io.nuls.mapping.business;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.nuls.mapping.dao.mapper.AddressMapper;
import io.nuls.mapping.dao.mapper.EthTradeMapper;
import io.nuls.mapping.dao.mapper.NulsTradeMapper;
import io.nuls.mapping.dao.util.SearchOperator;
import io.nuls.mapping.dao.util.Searchable;
import io.nuls.mapping.entity.Address;
import io.nuls.mapping.entity.EthTrade;
import io.nuls.mapping.entity.NulsTrade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class TradeBusiness {

    @Autowired
    private EthTradeMapper ethTradeMapper;

    @Autowired
    private NulsTradeMapper nulsTradeMapper;

    @Autowired
    private AddressMapper addressMapper;

    /**
     * 根据用户的ETH地址，查询ETH20代币的转入流水记录
     *
     * @param toEthAddress
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public PageInfo<EthTrade> getEthTradeList(String toEthAddress, int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        Searchable searchable = new Searchable();
        searchable.addCondition("to_address", SearchOperator.eq, toEthAddress);
        PageHelper.orderBy("create_time desc");
        List<EthTrade> list = ethTradeMapper.selectList(searchable);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }


    /**
     * 根据用户的Nuls地址，查询Nuls代币的转账流水记录
     */
    public PageInfo<NulsTrade> getNulsTradeList(String nulsAddress, int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        Searchable searchable = new Searchable();
        searchable.addCondition("nuls_address", SearchOperator.eq, nulsAddress);
        PageHelper.orderBy("create_time desc");
        List<EthTrade> list = ethTradeMapper.selectList(searchable);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }

    /**
     * 添加未确认Eth交易流水
     */
    @Transactional
    public void saveEthTrade(EthTrade ethTrade) {
        ethTrade.setCreateTime(new Date(System.currentTimeMillis()));
        ethTrade.setStatus(0);
        ethTradeMapper.insert(ethTrade);
    }

    /**
     * 确认Eth交易流水，
     * 确认后需要发送我们的nuls代币给账户的nuls地址
     *
     * @param ethTxHash
     */
    @Transactional
    public void confirmEthTrade(String ethTxHash) {
        EthTrade ethTrade = ethTradeMapper.selectByPrimaryKey(ethTxHash);
        if (ethTrade == null) {
            throw new NullPointerException("eth tx trade not found");
        }
        Searchable searchable = new Searchable();
        searchable.addCondition("eth_address", SearchOperator.eq, ethTrade.getFromAddress());
        Address address = addressMapper.selectBySearchable(searchable);
        if (address == null) {
            throw new NullPointerException("account not found");
        }
        //确认交易
        ethTrade.setStatus(1);
        ethTradeMapper.updateByPrimaryKey(ethTrade);
        //TODO 发送nuls交易，并存储
        // NulsTrade nulsTrade = NulsTool.sentNulsTx();
        NulsTrade nulsTrade = new NulsTrade();
        nulsTrade.setCreateTime(new Date(System.currentTimeMillis()));
        nulsTrade.setStatus(0);
        nulsTradeMapper.insert(nulsTrade);

    }

    /**
     * 确认nuls交易
     *
     * @param nulsTxHash
     */
    @Transactional
    public void confirmNulsTrade(String nulsTxHash) {
        NulsTrade nulsTrade = nulsTradeMapper.selectByPrimaryKey(nulsTxHash);
        if (nulsTrade == null) {
            throw new NullPointerException("eth tx trade not found");
        }
        Address address = addressMapper.selectByPrimaryKey(nulsTrade.getNulsAddress());
        if (address == null) {
            throw new NullPointerException("account not found");
        }

        nulsTrade.setStatus(1);
        nulsTradeMapper.updateByPrimaryKey(nulsTrade);
        //修改用户总兑换记录
        address.setTotalCount(address.getTotalCount() + 1);
        address.setTotalAmount(address.getTotalAmount() + nulsTrade.getAmount());
    }


}
