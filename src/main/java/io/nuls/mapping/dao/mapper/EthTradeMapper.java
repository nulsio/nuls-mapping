package io.nuls.mapping.dao.mapper;

import io.nuls.mapping.dao.util.Searchable;
import io.nuls.mapping.entity.EthTrade;

import java.util.List;

@MyBatisMapper
public interface EthTradeMapper {

    int deleteByPrimaryKey(String id);

    int insert(EthTrade record);

    int insertSelective(String record);

    EthTrade selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(EthTrade record);

    int updateByPrimaryKey(EthTrade record);

    List<EthTrade> selectList(Searchable searchable);
}