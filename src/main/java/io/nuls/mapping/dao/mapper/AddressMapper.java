package io.nuls.mapping.dao.mapper;

import io.nuls.mapping.dao.util.Searchable;
import io.nuls.mapping.entity.Address;

@MyBatisMapper
public interface AddressMapper {

    int deleteByPrimaryKey(String nulsAddress);

    int insert(Address record);

    int insertSelective(Address record);

    Address selectByPrimaryKey(String nulsAddress);

    int updateByPrimaryKeySelective(Address record);

    int updateByPrimaryKey(Address record);

    Address selectBySearchable(Searchable searchable);
}