package io.nuls.mapping.dao.mapper;

import io.nuls.mapping.entity.NulsTrade;

@MyBatisMapper
public interface NulsTradeMapper {

    int deleteByPrimaryKey(String id);

    int insert(NulsTrade record);

    int insertSelective(NulsTrade record);

    NulsTrade selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(NulsTrade record);

    int updateByPrimaryKey(NulsTrade record);
}