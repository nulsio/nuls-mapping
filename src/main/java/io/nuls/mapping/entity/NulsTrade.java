package io.nuls.mapping.entity;

import java.util.Date;

public class NulsTrade {
    private String txHash;

    private String nulsAddress;

    private String ethTxHash;

    private Date createTime;

    private Long amount;

    private Integer status;

    public String getTxHash() {
        return txHash;
    }

    public void setTxHash(String txHash) {
        this.txHash = txHash == null ? null : txHash.trim();
    }

    public String getNulsAddress() {
        return nulsAddress;
    }

    public void setNulsAddress(String nulsAddress) {
        this.nulsAddress = nulsAddress == null ? null : nulsAddress.trim();
    }

    public String getEthTxHash() {
        return ethTxHash;
    }

    public void setEthTxHash(String ethTxHash) {
        this.ethTxHash = ethTxHash == null ? null : ethTxHash.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}