package io.nuls.mapping.entity;

public class Address {
    private String nulsAddress;

    private String ethAddress;

    private Long totalAmount;

    private Integer totalCount;

    public String getNulsAddress() {
        return nulsAddress;
    }

    public void setNulsAddress(String nulsAddress) {
        this.nulsAddress = nulsAddress == null ? null : nulsAddress.trim();
    }

    public String getEthAddress() {
        return ethAddress;
    }

    public void setEthAddress(String ethAddress) {
        this.ethAddress = ethAddress == null ? null : ethAddress.trim();
    }

    public Long getTotalAmount() {
        if (totalAmount == null) {
            totalAmount = 0L;
        }
        return totalAmount;
    }

    public void setTotalAmount(Long totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getTotalCount() {
        if (totalCount == null) {
            totalCount = 0;
        }
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }
}