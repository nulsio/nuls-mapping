package io.nuls.test;

import io.nuls.mapping.business.AddressBusiness;
import io.nuls.mapping.entity.Address;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:ApplicationContext.xml")
public class MybatisTest {

    @Autowired
    private AddressBusiness addressBusiness;

    @Test
    public void testGetAddress() {
        Address address = addressBusiness.getByNulsAddress("aaa");
        System.out.println(address.getEthAddress());
    }
}
